from selenium import webdriver
import time
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options

binary = r'C:\Users\Beda Pudja\AppData\Local\Mozilla Firefox\firefox.exe'
options = Options()
options.set_headless(headless=True)
options.binary = binary
cap = DesiredCapabilities().FIREFOX
cap["marionette"] = True
driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path="C:\\Browser_Drivers\\geckodriver.exe")
driver.get('http://127.0.0.1:8000/')
time.sleep(2)

acc1 = driver.find_element_by_id('acc1')
acc2 = driver.find_element_by_id('acc2')
acc3 = driver.find_element_by_id('acc3')
acc4 = driver.find_element_by_id('acc4')

# When clicked
acc1.click()
assert "learning online" in driver.page_source
time.sleep(2)

acc2.click()
assert "college" in driver.page_source
time.sleep(2)

acc3.click()
assert "PEMIRA" in driver.page_source
time.sleep(2)

acc4.click()
assert "(yet)" in driver.page_source
time.sleep(2)

print("OK")

time.sleep(5)
driver.quit()