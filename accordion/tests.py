from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
import unittest
from .views import index
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options
import time


# Create your tests here.
class UnitTest(TestCase):

    def test_main_url_exists(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_return_main_html(self):
        response = Client().get("")
        self.assertTemplateUsed(response, "accordion/main.html")

    def test_main_view_func(self):
        found = resolve("/")
        self.assertEqual(found.func, index)

# class FunctionalityTest(unittest.TestCase):

#     def setUp(self):
#         self.driver = webdriver.Firefox()

#     def tearDown(self):
#         self.driver.quit()

#     def test_text_detected_when_buttons_are_clicked(self):
#         binary = r'C:\Users\Beda Pudja\AppData\Local\Mozilla Firefox\firefox.exe'
#         options = Options()
#         options.set_headless(headless=True)
#         options.binary = binary
#         cap = DesiredCapabilities().FIREFOX
#         cap["marionette"] = True
#         driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path="C:\\Browser_Drivers\\geckodriver.exe")
#         driver.get('http://127.0.0.1:8000/')
#         time.sleep(2)
        

#         acc1 = self.driver.find_element_by_id('acc1')
#         acc2 = self.driver.find_element_by_id('acc2')
#         acc3 = self.driver.find_element_by_id('acc3')
#         acc4 = self.driver.find_element_by_id('acc4')


# cannot run functionality test in test.py
# with chrome comes the exception
# selenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {"method":"css selector","selector":"[id="acc1"]"}

# with firefox comes the exception
# selenium.common.exceptions.SessionNotCreatedException: Message: Unable to find a matching set of capabilities